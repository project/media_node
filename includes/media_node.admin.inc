<?php

/**
 * @file
 * Administrative functions for Media: Node.
 */

/**
 * The administrative settings form for Media: Node.
 */
function media_node_settings() {
  $form = media_node_admin_form();
  $form['settings_info'] = array(
    '#type' => 'item',
    '#value' => t('These settings specifically affect media extracted from a node reference.'),
    '#weight' => -10,
  );
  return system_settings_form($form);
}

/**
 * This form will be displayed both at /admin/settings/media_node and
 * admin/content/emfield.
 */
function media_node_admin_form() {
  $form = array();

  return $form;
}
