<?php

/**
 * @file
 * Media: Node provider object.
 */

class MediaNode extends EmapiMedia {
  // The provider; matches the scheme:// of the $uri.
  public $provider = 'node';

  public $identifier = 'nid';

  // Regex patterns for matching while parsing a URL.
  public $patterns = array(
    '@\[\[nid:(\d+)\]\]@i',
    '@^node/(\d+)$@',
  );

  // Use these for building the URL to the original location.
  public function get_prefix_url() {
    return 'node/';
  }

  public function parse($original) {
    global $base_url;
    $path = drupal_get_normal_path(preg_replace('@^'. $base_url . '/@', '', $original));
    if ($match = parent::parse($path)) {
      $this->set_original($original);
      return $match;
    }
    return parent::parse($original);
  }
}
